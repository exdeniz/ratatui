$('.sheduleSliders').slick({
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
    speed: 1000,
    slidesToShow: 4,
    slidesToScroll: 1,
    infinite: false,
    draggable: false
});




$('.sheduleSlide')
    .on("mouseenter", $(this), function() {
            var position = $(this).offset();
            $('body').append('<div class="slideShadow"></div>')
            $('.slideShadow').css({
                'width' : $(this).width(),
                'height': $(this).height(),
                'top' : position.top,
                'left': position.left,
                'pointer-events' : 'none',
                'display' : 'none'
            })
            $('.slideShadow').fadeIn(500)

        })
    .on("mouseleave", $(this), function() {
            $('.slideShadow').fadeOut(500)
            $('.slideShadow').remove()
        })
