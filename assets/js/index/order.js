$(".orderFormItemPayItem").click(function () {
    $('.orderFormItemPayItem').removeClass('orderFormItemPayItemActive')
    $(this).addClass('orderFormItemPayItemActive')
    $(this).find('input').iCheck('check');
})


$('.orderFormItemPayItem input').on('ifChecked', function(event){
  $('.orderFormItemPayItem').removeClass('orderFormItemPayItemActive')
  $(this).closest('.orderFormItemPayItem').addClass('orderFormItemPayItemActive')
});

$('.js-showOrderComplite').click(function () {
    $('.orderForm').fadeOut()
    $('.orderComplite').fadeIn()
})
