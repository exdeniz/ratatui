$(".js-closeCart").click(function () {
    $(".cartAdd").fadeOut();
})

$(".js-showCart").click(function () {
    $(".cartAdd").fadeIn();
})

$(".cartSummary").stick_in_parent();

$('.js-contactPopup').click(function() {
    $('.overley').addClass('overleyShow')
    $('.contactPopup').addClass('contactPopupShow')
    $('body').css('position', 'fixed')
})

$('.js-feedbackFull').click(function () {
    //$('.overley').addClass('overleyShow')
    $('.feedbackItemFull').fadeIn();
    // $('body').css('position', 'fixed')
})

$('.js-feedbackFullClose').click(function () {
    //$('.overley').addClass('overleyShow')
    $('.feedbackItemFull').fadeOut();
    // $('body').css('position', 'fixed')
})
$('.js-feedbackPopup').click(function() {
    $('.overley').addClass('overleyShow')
    $('.feedbackPopup').addClass('feedbackPopupShow')
    $('body').css('position', 'fixed')
})

$('.lessonCalendarWeekDay').matchHeight({
    byRow: true,
    property: 'height',
    target: null,
    remove: false
});

// setTimeout(function() {
//     $('.lessonCalendarWeekDay').matchHeight();
// }, 200);

$('.sheduleSliders').slick({
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
    speed: 1000,
    slidesToShow: 4,
    slidesToScroll: 1,
    infinite: false,
    draggable: false
});




$('.sheduleSlide')
    .on("mouseenter", $(this), function() {
            var position = $(this).offset();
            $('body').append('<div class="slideShadow"></div>')
            $('.slideShadow').css({
                'width' : $(this).width(),
                'height': $(this).height(),
                'top' : position.top,
                'left': position.left,
                'pointer-events' : 'none',
                'display' : 'none'
            })
            $('.slideShadow').fadeIn(500)

        })
    .on("mouseleave", $(this), function() {
            $('.slideShadow').fadeOut(500)
            $('.slideShadow').remove()
        })

$(".orderFormItemPayItem").click(function () {
    $('.orderFormItemPayItem').removeClass('orderFormItemPayItemActive')
    $(this).addClass('orderFormItemPayItemActive')
    $(this).find('input').iCheck('check');
})


$('.orderFormItemPayItem input').on('ifChecked', function(event){
  $('.orderFormItemPayItem').removeClass('orderFormItemPayItemActive')
  $(this).closest('.orderFormItemPayItem').addClass('orderFormItemPayItemActive')
});

$('.js-showOrderComplite').click(function () {
    $('.orderForm').fadeOut()
    $('.orderComplite').fadeIn()
})


$('.promoSlider').slick({
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
    speed: 1000,
    draggable: false
});






$('.gallerySlider').slick({
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    centerMode: true,
    variableWidth: true,
    initialSlide: 5,
    draggable: false
});


$('.partnerSlider').slick({
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
    infinite: true,
    speed: 300,
    slidesToShow: 6,
    variableWidth: true,
    draggable: false
});

$('.feedbackPromoSlider').slick({
    cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
    speed: 1000,
    infinite: true,
    draggable: false
});

$('input').iCheck({
    labelHover: false,
    cursor: true
});

// $('.promoSlider').on('beforeChange', function(event, slick, nextSlide){
//     $('.promoSlideText').fadeOut()
//     $('.promoSlide').eq(nextSlide).children('.promoSlideText').fadeIn();
// });

$('.menuItem').hover(function() {
    var obj = $(this);
    var childPos = obj.offset();
    var parentPos = obj.parent().offset();
    var globalPos = obj.parent().parent().offset();
    var childOffset = {
        top: childPos.top - parentPos.top + globalPos.top,
        left: globalPos.left + childPos.left
    };
    $(this).css("background-position", -childOffset.left);
    $(this).children(".menuItemDropdown").toggleClass("menuItemDropdownOpen")
});


$('.js-closeoverlay').click(function() {
    $('.overley').removeClass('overleyShow')
    $('.login').removeClass('loginShow')
    $('.registration').removeClass('registrationShow')
    $('.recovery').removeClass('recoveryShow')
    $('body').css('position', 'relative')
})

$('.js-loginopen').click(function() {
    $('.overley').addClass('overleyShow')
    $('.login').addClass('loginShow')
    $('body').css('position', 'fixed')
})

$('.js-registrationopen').click(function() {
    $('.overley').addClass('overleyShow')
    $('.registration').addClass('registrationShow')
    $('body').css('position', 'fixed')
})

$('.js-registrationswith').click(function() {
    $('.login').removeClass('loginShow')
    $('.registration').addClass('registrationShow')
})

$('.js-recoveryswith').click(function() {
    $('.login').removeClass('loginShow')
    $('.recovery').addClass('recoveryShow')
})
$('.js-inputphone').inputmask("mask", {
    "mask": "+7 (999) 999-9999",
    "showMaskOnFocus": true,
    "showMaskOnHover": true,
    "clearMaskOnLostFocus": false
});

$('.selectric').selectric();

$(".js-showcabinet").click(function() {
    $(".cabinetTabFormEdit").hide()
    $(".cabinetTabForm").fadeIn()
})

$(".js-showcabinetedit").click(function() {
    $(".cabinetTabForm").hide()
    $(".cabinetTabFormEdit").fadeIn()
})

$(".js-cabinetItemMore").click(function() {
    var actualHeight = $(this).parents(".cabinetOrderItems").prop('scrollHeight');
    var cssHeight = $(this).parents(".cabinetOrderItems").css('max-height')
    var elem = $(this).parents(".cabinetOrderItems")
    var elemOpen = elem.hasClass('cabinetOrderItemsAll')
    console.log(elemOpen)
    if (!elem.hasClass('cabinetOrderItemsAll'))
        elem.css('max-height', actualHeight + 15)
    if (elem.hasClass('cabinetOrderItemsAll'))
        elem.css('max-height', '')

    elem.toggleClass("cabinetOrderItemsAll")

})

 $(".js-tabbutton").click(function(event) {
        event.preventDefault();
        var parentClassName = $(this).parent().attr('class').split(' ')[0] + 'Active';
        var activeClass = $(this).parent().attr('class').split(' ')[0];


        $(this).parent().siblings().removeClass("cabinetTabButtonLKActive");
        $(this).parent().siblings().removeClass("cabinetTabButtonOrdersActive");
        $(this).parent().siblings().removeClass("cabinetTabButtonCartActive");
        $(this).parent().addClass(parentClassName);
        var tab = $(this).attr("href");
        $(".cabinetTab").not(tab).css("display", "none");
        $(".cabinetTabNoBG").not(tab).css("display", "none");
        $(tab).fadeIn();
    });




$(".photoGalleryItems").lightGallery();
